import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    logo: {
      paddingBottom: 60
    },
    buttons: { 
      paddingTop: 60 
    },
    emailInput: {
      marginBottom: 10,
      height: 40,
      width: 300,
      borderColor: 'gray',
      borderWidth: 1
    },
    passwordInput: {
      height: 40, 
      width: 300, 
      borderColor: 'gray', 
      borderWidth: 1
    }
  });