import React from 'react';
import { TextInput, View, Image, KeyboardAvoidingView, Button } from 'react-native';
import { styles } from "./login.styles";

export default class Login extends React.Component {
  state = {
    login: '',
    password: ''
  };

  doLogin() {
    const { login, password } = this.state;
    alert(`Login: ${login} Password: ${password}`);
    this.props.navigation.navigate('Profile');
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.logo}>
          <Image
            source={require('../../../assets/logo.png')}
          />
        </View>

        <View>
          <TextInput
            style={styles.emailInput}
            onChangeText={(login) => this.setState({ login })}
            value={this.state.login}
            autoCapitalize={'none'}
            keyboardType={'email-address'}
          />
          <TextInput
            style={styles.passwordInput}
            onChangeText={(password) => this.setState({ password })}
            value={this.state.password}
            secureTextEntry={true}
          />
        </View>

        <View style={styles.buttons}>
          <Button
            onPress={() => {
              this.doLogin();
            }}
            title="Login"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}
