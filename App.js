import {
  createStackNavigator,
} from 'react-navigation';

import Login from './src/pages/login/login';
import Profile from './src/pages/profile/profile';

const App = createStackNavigator({
  Home: { 
    title: 'Login',
    screen: Login 
  },
  Profile: { 
    screen: Profile,
    title: 'Profile',
    headerLeft: null
  },
});

export default App;
